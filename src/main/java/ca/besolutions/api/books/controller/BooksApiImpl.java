package ca.besolutions.api.books.controller;

import ca.besolutions.api.books.model.Book;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class BooksApiImpl implements BooksApi {
    @Override
    public ResponseEntity<Book> getBook(Long bookId) {
        return null;
    }

    @Override
    public ResponseEntity<Book> putBook(@Valid Book book) {
        throw new NotImplementedException("Put Book");
    }

    //    @Override
//    public CompletableFuture<ResponseEntity<Void>> addBook(@Valid Book body) {
//        throw new NotImplementedException("Adding a book is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<Void>> deleteBook(Long bookId, String apiKey) {
//        throw new NotImplementedException("Delete a book is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<List<Book>>> findBooksByStatus(@NotNull @Valid List<String> status) {
//        throw new NotImplementedException("Find a book by status is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<List<Book>>> findBooksByTags(@NotNull @Valid List<String> tags) {
//        throw new NotImplementedException("Find a book by tags is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<Book>> getBookById(Long bookId) {
//        throw new NotImplementedException("Get a specific book is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<Void>> updateBook(@Valid Book body) {
//        throw new NotImplementedException("Adding a book is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<Void>> updateBookWithForm(Long bookId, String name, String status) {
//        throw new NotImplementedException("Adding a book is not yet implemented!");
//    }
//
//    @Override
//    public CompletableFuture<ResponseEntity<ModelApiResponse>> uploadFile(Long bookId, String additionalMetadata, @Valid MultipartFile file) {
//        throw new NotImplementedException("Adding a book is not yet implemented!");
//    }
}
